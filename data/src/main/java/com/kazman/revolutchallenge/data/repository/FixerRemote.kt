package com.kazman.revolutchallenge.data.repository

import com.kazman.revolutchallenge.data.entity.ExchangeRateEntity
import io.reactivex.Observable

interface FixerRemote {
    fun getRates(baseCurrency: String): Observable<ExchangeRateEntity>
}