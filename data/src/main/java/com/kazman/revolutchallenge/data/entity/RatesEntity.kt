package com.kazman.revolutchallenge.data.entity

data class RatesEntity(val GBP: Double, val EUR: Double, val USD: Double)