package com.kazman.revolutchallenge.data

import com.kazman.revolutchallenge.data.extension.toModel
import com.kazman.revolutchallenge.data.repository.FixerRemote
import com.kazman.revolutchallenge.domain.model.CalculateParams
import com.kazman.revolutchallenge.domain.model.ExchangeRateModel
import com.kazman.revolutchallenge.domain.repository.RevolutRepository
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RevolutDataRepository @Inject constructor(private val fixerRemote: FixerRemote) : RevolutRepository {
    override fun calculateRate(inputValue: Double, outputValue: Double, exchangeRate: Double): Single<CalculateParams> {
        return Single.create {
            if (outputValue == 0.0) {
                it.onSuccess(CalculateParams(inputValue, inputValue * exchangeRate, exchangeRate))
            } else {
                it.onSuccess(CalculateParams(outputValue / exchangeRate, outputValue, exchangeRate))
            }
        }
    }

    override fun getRates(baseCurrency: String): Observable<ExchangeRateModel> {
        return Observable.interval(0, 30, TimeUnit.SECONDS)
                .flatMap {
                    fixerRemote.getRates(baseCurrency)
                            .map { it.toModel() }
                }
    }

}