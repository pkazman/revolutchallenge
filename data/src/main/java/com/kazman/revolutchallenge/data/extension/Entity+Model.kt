package com.kazman.revolutchallenge.data.extension

import com.kazman.revolutchallenge.data.entity.ExchangeRateEntity
import com.kazman.revolutchallenge.data.entity.RatesEntity
import com.kazman.revolutchallenge.domain.model.ExchangeRateModel
import com.kazman.revolutchallenge.domain.model.RatesModel

fun RatesEntity.toModel(): RatesModel {
    return RatesModel(this.GBP, this.EUR, this.USD)
}

fun RatesModel.toEntity(): RatesEntity {
    return RatesEntity(this.GBP, this.EUR, this.USD)
}

fun ExchangeRateEntity.toModel(): ExchangeRateModel {
    return ExchangeRateModel(this.base, this.date, this.rates.toModel())
}

fun ExchangeRateModel.toEntity(): ExchangeRateEntity {
    return ExchangeRateEntity(this.base, this.date, this.rates.toEntity())
}