package com.kazman.revolutchallenge.data.entity

data class ExchangeRateEntity(val base: String, val date: String, val rates: RatesEntity)