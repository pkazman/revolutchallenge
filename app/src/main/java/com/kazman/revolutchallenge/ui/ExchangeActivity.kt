package com.kazman.revolutchallenge.ui

import android.arch.lifecycle.LifecycleActivity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.widget.EditText
import android.widget.RadioGroup
import com.jakewharton.rxbinding2.widget.RxRadioGroup
import com.jakewharton.rxbinding2.widget.RxTextView
import com.kazman.revolutchallenge.R
import com.kazman.revolutchallenge.injection.extensions.toast
import com.kazman.viewmodel.ViewModelFactory
import com.kazman.viewmodel.data.CurrencyType
import com.kazman.viewmodel.data.Output
import com.kazman.viewmodel.data.Resource
import com.kazman.viewmodel.data.ResourceState
import com.kazman.viewmodel.exchange.ExchangeViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_exchange.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ExchangeActivity : LifecycleActivity() {
    @Inject lateinit var viewModelFactory: ViewModelFactory
    private lateinit var exchangeViewModel: ExchangeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange)
        AndroidInjection.inject(this)

        exchangeViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ExchangeViewModel::class.java)

        subscribeToEditTexts(etInput, exchangeViewModel::onInputValueChanged)
        subscribeToEditTexts(etOutput, exchangeViewModel::onOutputValueChanged)
        subscribeToRadioGroup(inputRadioGroup, exchangeViewModel::onInputCurrencyChanged)
        subscribeToRadioGroup(outputRadioGroup, exchangeViewModel::onOutputCurrencyChanged)
    }

    private fun subscribeToRadioGroup(radioGroup: RadioGroup, instruction: (CurrencyType) -> Unit) {
        RxRadioGroup.checkedChanges(radioGroup)
                .skipInitialValue()
                .subscribe {
                    val currencyType = when (it) {
                        R.id.inputUsd, R.id.outputUsd -> CurrencyType.USD
                        R.id.inputEur, R.id.outputEur -> CurrencyType.EUR
                        R.id.inputGbp, R.id.outputGbp -> CurrencyType.GBP
                        else -> CurrencyType.UNKNOWN
                    }
                    instruction(currencyType)
                }
    }

    override fun onStart() {
        super.onStart()
        exchangeViewModel.exchangeRateLiveData.observe(this, Observer<Resource<Output>> { resource ->
            resource?.let {
                when (it.status) {
                    ResourceState.LOADING -> {
                    }
                    ResourceState.OUTPUT_SUCCESS -> {
                        etOutput.setText("%.2f".format(it.data?.output))
                    }
                    ResourceState.INPUT_SUCCESS -> {
                        etInput.setText("%.2f".format(it.data?.input))
                    }
                    ResourceState.ERROR -> {
                        toast("Something went wrong")
                    }
                }
            }
        })
    }

    private fun subscribeToEditTexts(editText: EditText, instruction: (Double) -> Unit) {
        RxTextView.textChanges(editText)
                .debounce(300, TimeUnit.MILLISECONDS)
                .map { if (it.isEmpty()) 0.0 else it.toString().toDouble() }
                .subscribe {
                    if (currentFocus == editText)
                        instruction(it)
                }
    }
}