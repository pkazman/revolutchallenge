package com.kazman.revolutchallenge.injection.files

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApp

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity
