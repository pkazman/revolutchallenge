package com.kazman.revolutchallenge.injection

import android.app.Application
import com.kazman.revolutchallenge.RevolutApplication
import com.kazman.revolutchallenge.injection.files.PerApp
import com.kazman.revolutchallenge.injection.module.ApplicationModule
import com.kazman.revolutchallenge.injection.module.ActivityBindingModule
import com.kazman.revolutchallenge.injection.module.DataModule
import com.kazman.revolutchallenge.injection.module.FixerModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule


@PerApp
@Component(modules = arrayOf(ApplicationModule::class,
        FixerModule::class,
        DataModule::class,
        ActivityBindingModule::class,
        AndroidSupportInjectionModule::class))
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: RevolutApplication)
}