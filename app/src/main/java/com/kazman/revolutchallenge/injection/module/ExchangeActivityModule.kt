package com.kazman.revolutchallenge.injection.module

import com.kazman.revolutchallenge.domain.interactor.CalculateRate
import com.kazman.revolutchallenge.domain.interactor.GetExchangeRates
import com.kazman.viewmodel.ViewModelFactory
import dagger.Module
import dagger.Provides

@Module
open class ExchangeActivityModule {
    @Provides
    fun provideViewModelFactory(getExchangeRates: GetExchangeRates, calculateRate: CalculateRate): ViewModelFactory {
        return ViewModelFactory(getExchangeRates, calculateRate)
    }
}