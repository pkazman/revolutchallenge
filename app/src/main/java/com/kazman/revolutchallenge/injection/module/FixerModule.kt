package com.kazman.revolutchallenge.injection.module

import com.kazman.revolutchallenge.data.repository.FixerRemote
import com.kazman.revolutchallenge.fixer.FixerRemoteImpl
import com.kazman.revolutchallenge.fixer.FixerService
import com.kazman.revolutchallenge.injection.files.PerApp
import com.kazman.revolutchallenge.utils.ConnectivityInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class FixerModule {
    @PerApp
    @Provides
    fun providesFixerService(connectivityInterceptor: ConnectivityInterceptor): FixerService {
        val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.addInterceptor(connectivityInterceptor)

        val retrofitBuild = Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

        retrofitBuild.client(httpClient.build())

        val retrofit = retrofitBuild.build()

        return retrofit.create(FixerService::class.java)
    }

    @Provides
    @PerApp
    internal fun provideFixerRemote(fixerService: FixerService): FixerRemote {
        return FixerRemoteImpl(fixerService)
    }
}