package com.kazman.revolutchallenge.injection.module

import com.kazman.revolutchallenge.data.RevolutDataRepository
import com.kazman.revolutchallenge.data.repository.FixerRemote
import com.kazman.revolutchallenge.domain.repository.RevolutRepository
import com.kazman.revolutchallenge.injection.files.PerApp
import dagger.Module
import dagger.Provides

@Module
class DataModule {

    @Provides
    @PerApp
    internal fun provideRevolutRepository(fixerRemote: FixerRemote): RevolutRepository {
        return RevolutDataRepository(fixerRemote)
    }
}