package com.kazman.revolutchallenge.injection.module

import android.app.Application
import android.content.Context
import com.kazman.revolutchallenge.UiThread
import com.kazman.revolutchallenge.data.executor.JobExecutor
import com.kazman.revolutchallenge.domain.executor.PostExecutionThread
import com.kazman.revolutchallenge.domain.executor.ThreadExecutor
import com.kazman.revolutchallenge.injection.files.PerApp
import dagger.Module
import dagger.Provides

@Module
open class ApplicationModule {
    @Provides
    @PerApp
    fun provideContext(application: Application): Context {
        return application
    }
    @Provides
    @PerApp
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApp
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

}