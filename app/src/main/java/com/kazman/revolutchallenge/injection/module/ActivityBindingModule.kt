package com.kazman.revolutchallenge.injection.module

import com.kazman.revolutchallenge.injection.files.PerActivity
import com.kazman.revolutchallenge.ui.ExchangeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(ExchangeActivityModule::class))
    abstract fun bindMainActivity(): ExchangeActivity

}