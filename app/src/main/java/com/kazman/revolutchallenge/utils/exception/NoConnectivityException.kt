package com.kazman.revolutchallenge.utils.exception

class NoConnectivityException(message: String = "No internet connection") : Throwable(message)