package com.kazman.revolutchallenge.utils

import android.content.Context
import com.kazman.revolutchallenge.utils.exception.NoConnectivityException
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ConnectivityInterceptor @Inject constructor(private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response? {

        if (!NetworkUtil.isOnline(context)) {
            throw NoConnectivityException("Application is offline, please check your connection status.")
        }

        val builder = chain!!.request()!!.newBuilder()
        return chain.proceed(builder.build())
    }

}