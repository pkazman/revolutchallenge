package com.kazman.revolutchallenge.fixer

import com.kazman.revolutchallenge.data.entity.ExchangeRateEntity
import com.kazman.revolutchallenge.data.repository.FixerRemote
import io.reactivex.Observable
import javax.inject.Inject

class FixerRemoteImpl @Inject constructor(private val fixerService: FixerService) : FixerRemote {
    override fun getRates(baseCurrency: String): Observable<ExchangeRateEntity> {
        return fixerService.getLatest(baseCurrency)
    }

}