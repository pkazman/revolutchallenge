package com.kazman.revolutchallenge.fixer

import com.kazman.revolutchallenge.data.entity.ExchangeRateEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface FixerService {

    @GET("latest")
    fun getLatest(@Query("base") base: String): Observable<ExchangeRateEntity>

}