package com.kazman.revolutchallenge.domain.model

class CalculateParams(val inputValue: Double, val outputValue: Double, val exchangeRate: Double) {

}