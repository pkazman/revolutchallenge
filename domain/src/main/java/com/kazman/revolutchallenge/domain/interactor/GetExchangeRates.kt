package com.kazman.revolutchallenge.domain.interactor

import com.kazman.revolutchallenge.domain.executor.PostExecutionThread
import com.kazman.revolutchallenge.domain.executor.ThreadExecutor
import com.kazman.revolutchallenge.domain.model.ExchangeRateModel
import com.kazman.revolutchallenge.domain.model.ExchangeRateParams
import com.kazman.revolutchallenge.domain.repository.RevolutRepository
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetExchangeRates @Inject constructor(val repository: RevolutRepository,
                                           private val threadExecutor: ThreadExecutor,
                                           private val postExecutionThread: PostExecutionThread)
    : BaseUseCase() {

    private fun buildUseCaseObservable(params: ExchangeRateParams): Observable<ExchangeRateModel> {
        return repository.getRates(params.baseCurrency)
    }

    fun execute(observer: DisposableObserver<ExchangeRateModel>, params: ExchangeRateParams) {
        val observable = this.buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler) as Observable<ExchangeRateModel>

        addDisposable(observable.subscribeWith(observer))
    }

}