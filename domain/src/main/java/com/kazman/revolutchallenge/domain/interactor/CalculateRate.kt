package com.kazman.revolutchallenge.domain.interactor

import com.kazman.revolutchallenge.domain.executor.PostExecutionThread
import com.kazman.revolutchallenge.domain.executor.ThreadExecutor
import com.kazman.revolutchallenge.domain.model.CalculateParams
import com.kazman.revolutchallenge.domain.repository.RevolutRepository
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CalculateRate @Inject constructor(private val repository: RevolutRepository,
                                        private val threadExecutor: ThreadExecutor,
                                        private val postExecutionThread: PostExecutionThread) : BaseUseCase() {

    private fun buildUseCaseSingle(params: CalculateParams): Single<CalculateParams> {
        return repository.calculateRate(params.inputValue, params.outputValue, params.exchangeRate)
    }

    fun execute(observer: DisposableSingleObserver<CalculateParams>, params: CalculateParams) {
        val observable = this.buildUseCaseSingle(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler) as Single<CalculateParams>

        addDisposable(observable.subscribeWith(observer))
    }
}