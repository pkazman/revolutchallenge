package com.kazman.revolutchallenge.domain.model

data class ExchangeRateModel(val base: String, val date: String, val rates: RatesModel)