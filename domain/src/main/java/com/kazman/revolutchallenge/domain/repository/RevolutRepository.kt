package com.kazman.revolutchallenge.domain.repository

import com.kazman.revolutchallenge.domain.model.CalculateParams
import com.kazman.revolutchallenge.domain.model.ExchangeRateModel
import io.reactivex.Observable
import io.reactivex.Single

interface RevolutRepository {
    fun getRates(baseCurrency: String): Observable<ExchangeRateModel>
    fun calculateRate(inputValue: Double, outputValue: Double, exchangeRate: Double): Single<CalculateParams>
}