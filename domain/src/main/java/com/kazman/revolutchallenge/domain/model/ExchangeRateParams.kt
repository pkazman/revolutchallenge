package com.kazman.revolutchallenge.domain.model

class ExchangeRateParams(val baseCurrency: String)