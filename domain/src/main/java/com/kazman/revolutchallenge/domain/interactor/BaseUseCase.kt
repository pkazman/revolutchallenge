package com.kazman.revolutchallenge.domain.interactor

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseUseCase {
    private var disposables = CompositeDisposable()

    fun dispose() {
        if (!disposables.isDisposed) disposables.dispose()
    }

    internal fun addDisposable(disposable: Disposable) {
        if (disposables.isDisposed)
            disposables = CompositeDisposable()
        disposables.add(disposable)
    }
}