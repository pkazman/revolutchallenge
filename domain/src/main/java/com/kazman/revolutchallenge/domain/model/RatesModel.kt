package com.kazman.revolutchallenge.domain.model

data class RatesModel(val GBP: Double, val EUR: Double, val USD: Double)