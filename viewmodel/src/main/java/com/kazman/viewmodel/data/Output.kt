package com.kazman.viewmodel.data

class Output(val input: Double, val output: Double, val exchangeRate: Double)