package com.kazman.viewmodel.data

enum class CurrencyType(val currencyCode: String) {
    USD("USD"), EUR("EUR"), GBP("GBP"), UNKNOWN("UNK")
}