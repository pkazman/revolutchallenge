package com.kazman.viewmodel.data

class Resource<out T>(val status: ResourceState, val data: T?) {
    fun <T> success(data: T): Resource<T> {
        return Resource(ResourceState.OUTPUT_SUCCESS, data)
    }

    fun <T> error(message: String, data: T?): Resource<T> {
        return Resource(ResourceState.ERROR, null)
    }

    fun <T> loading(): Resource<T> {
        return Resource(ResourceState.LOADING, null)
    }
}