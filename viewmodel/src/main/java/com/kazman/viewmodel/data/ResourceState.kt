package com.kazman.viewmodel.data

enum class ResourceState {
    LOADING, OUTPUT_SUCCESS, INPUT_SUCCESS, ERROR
}