package com.kazman.viewmodel.exchange

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.kazman.revolutchallenge.domain.interactor.CalculateRate
import com.kazman.revolutchallenge.domain.interactor.GetExchangeRates
import com.kazman.revolutchallenge.domain.model.CalculateParams
import com.kazman.revolutchallenge.domain.model.ExchangeRateModel
import com.kazman.revolutchallenge.domain.model.ExchangeRateParams
import com.kazman.viewmodel.data.CurrencyType
import com.kazman.viewmodel.data.Output
import com.kazman.viewmodel.data.Resource
import com.kazman.viewmodel.data.ResourceState
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ExchangeViewModel @Inject constructor(private val getExchangeRates: GetExchangeRates,
                                            private val calculateRate: CalculateRate)
    : ViewModel() {

    val exchangeRateLiveData = MutableLiveData<Resource<Output>>()
    private var inputValue = 0.0
    private var outputValue = 0.0
    private var exchangeRateModel: ExchangeRateModel? = null
    private var inputCurrency = CurrencyType.GBP
    private var outputCurrency = CurrencyType.EUR
    private var inputType: InputType = InputType.INPUT

    init {
        startGettingExchangeRates()
    }

    fun onInputValueChanged(value: Double) {
        inputType = InputType.INPUT
        inputValue = value
        calculateExchangeRate(inputValue, 0.0, getCurrentRate())
    }

    fun onOutputValueChanged(value: Double) {
        inputType = InputType.OUTPUT
        outputValue = value
        calculateExchangeRate(0.0, outputValue, getCurrentRate())
    }

    fun onInputCurrencyChanged(currencyType: CurrencyType) {
        inputCurrency = currencyType
        getExchangeRates.dispose()
        startGettingExchangeRates()
    }

    fun onOutputCurrencyChanged(currencyType: CurrencyType) {
        outputCurrency = currencyType
        calculateExchangeRate(inputValue, 0.0, getCurrentRate())
    }

    override fun onCleared() {
        super.onCleared()
        getExchangeRates.dispose()
    }

    private fun startGettingExchangeRates() {
        exchangeRateLiveData.postValue(Resource(ResourceState.LOADING, null))
        getExchangeRates.execute(GetExchangeRateSubscriber(), ExchangeRateParams(inputCurrency.currencyCode))
    }

    private fun calculateExchangeRate(input: Double, output: Double, exchangeRate: Double) {
        exchangeRateModel?.let {
            synchronized(it) {
                calculateRate.execute(CalculateExchangeRate(), CalculateParams(input, output, exchangeRate))
            }
        }
    }

    enum class InputType {
        INPUT, OUTPUT
    }

    inner class CalculateExchangeRate : DisposableSingleObserver<CalculateParams>() {
        override fun onError(e: Throwable) {

        }

        override fun onSuccess(params: CalculateParams) {
            exchangeRateLiveData.postValue(Resource(
                    if (inputType == InputType.INPUT)
                        ResourceState.OUTPUT_SUCCESS
                    else
                        ResourceState.INPUT_SUCCESS,
                    Output(params.inputValue, params.outputValue, params.exchangeRate)))
        }

    }

    inner class GetExchangeRateSubscriber : DisposableObserver<ExchangeRateModel>() {
        override fun onNext(exchangeRateModel: ExchangeRateModel) {
            this@ExchangeViewModel.exchangeRateModel = exchangeRateModel
            if (inputType == InputType.INPUT)
                calculateExchangeRate(inputValue, 0.0, getCurrentRate())
            else
                calculateExchangeRate(0.0, outputValue, getCurrentRate())
        }

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

        }
    }

    private fun getCurrentRate(): Double {
        val rate = when (outputCurrency) {
            CurrencyType.USD -> exchangeRateModel?.rates?.USD
            CurrencyType.EUR -> exchangeRateModel?.rates?.EUR
            CurrencyType.GBP -> exchangeRateModel?.rates?.GBP
            CurrencyType.UNKNOWN -> 1.0
        } ?: 1.0

        return if (rate == 0.0) {
            1.0
        } else {
            rate
        }
    }
}