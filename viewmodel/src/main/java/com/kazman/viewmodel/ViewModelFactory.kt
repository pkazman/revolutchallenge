package com.kazman.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.kazman.revolutchallenge.domain.interactor.CalculateRate
import com.kazman.revolutchallenge.domain.interactor.GetExchangeRates
import com.kazman.viewmodel.exchange.ExchangeViewModel

class ViewModelFactory(private val getExchangeRates: GetExchangeRates,
                       private val calculateExchange: CalculateRate)
    : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExchangeViewModel::class.java)) {
            return ExchangeViewModel(getExchangeRates, calculateExchange) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}